Module[
  {macOSPath = "/Applications/Wolfram Workbench.app/configuration/org.eclipse.osgi/bundles/13/1/.cp/MathematicaSourceVersioned/Head",
   workbenches = FileNames["WolframWorkbench", StringSplit[Environment["PATH"], ":"]],
   linuxJar},

  Switch[
    $SystemID,

    "MacOSX-x86-64",
    If[FileType[macOSPath] === Directory,
       Block[
         {$Path = Prepend[$Path, macOSPath]},
         << MUnit`],
       (* else *)
       Print["MUnit (or Wolfram Workbench) not found in "<>FileNameTake[macOSPath, 2]];
       Abort[]],

    "Linux-x86-64",
    If[workbenches === {},
       Print["MUnitRunner: WolframWorkbench: command not found"];
       Abort[]];
    linuxJar = FileNameJoin[{FileNameDrop[workbenches[[1]],-2],
                             "plugins/com.wolfram.eclipse.testing_2.0.126.jar"}];
    If[FileType[linuxJar] === File,
     Module[
       {dir = CreateDirectory[]},
       ExtractArchive[linuxJar, dir];
       Block[
         {$Path = Prepend[$Path, FileNameJoin[{dir, "MathematicaSourceVersioned/Head"}]]},
         << MUnit`];

       DeleteDirectory[dir, DeleteContents -> True]],
       (* else *)
       Print["MUnit not found in "<>FileNameDrop[linuxJar, -2]];
       Abort[]],

    _,
    Print["Unsupported operating system: "<>$SystemID];
    Abort[]]];
